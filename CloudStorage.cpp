//
// Created by bso on 18.02.18.
//

#include <iostream>
#include <boost/filesystem.hpp>

#include "CloudStorage.h"
#include "ProgramLog.h"
#include "Config.h"

CloudStorage::CloudStorage(const std::string &url, const std::string &local_storage_path):
		m_curl_wrapper(url, local_storage_path),
		m_storage_url(url),
		m_local_storage_path(local_storage_path) {
}

int CloudStorage::ProcessPull() {
	Json::Value storage_list;
	if(!m_curl_wrapper.GetRemoteStorageList(storage_list)) {
		return 1;
	}

	auto local_storage_list = getLocalStorageList();

	updateModifiedFiles(storage_list, local_storage_list);

	downloadDeletedFiles(storage_list, local_storage_list);

	std::cout << "Pull end" << std::endl;
	return 0;
}

int CloudStorage::ProcessPush() {
	Json::Value storage_list;
	if(!m_curl_wrapper.GetRemoteStorageList(storage_list)) {
		return 1;
	}

	auto local_storage_list = getLocalStorageList();

	uploadModifiedFiles(storage_list, local_storage_list);

	uploadNewFiles(storage_list, local_storage_list);

	sendInfoAboutDeletedFiles(storage_list, local_storage_list);

	std::cout << "Push end" << std::endl;
	return 0;
}

std::vector<std::pair<std::string, std::time_t>> CloudStorage::getRealLocalStorageList() {
	boost::filesystem::path local_storage(m_local_storage_path);

	std::vector<std::pair<std::string, std::time_t>> local_storage_list;
	boost::filesystem::directory_iterator end_iter;
	for(boost::filesystem::directory_iterator it(local_storage); it != end_iter; ++it) {
		if(boost::filesystem::is_directory(it->status())) {
			//TODO in future
			std::cout << "Couldn't process directory\"" <<
					  it->path().filename().string() << "\"" << std::endl;
		} else if(boost::filesystem::is_regular_file(it->status())) {
			auto pair = std::make_pair(it->path().filename().string(),
									   boost::filesystem::last_write_time(it->path()));
			local_storage_list.push_back(pair);
		} else {
			//TODO in future
			std::cout << "Couldn't process \"" <<
					  it->path().filename().string() << "\"" << std::endl;
		}
	}

	return local_storage_list;
}

void CloudStorage::updateModifiedFiles(
		const Json::Value &storage_list,
		const std::vector<std::pair<std::string, std::time_t>> &local_storage_list) {
	LOG_D("Start updateModifiedFiles()");
	auto list_of_identical_files = getListOfIdenticalFiles(storage_list, local_storage_list);

	for(const auto &it : list_of_identical_files) {
		if(it.last_write_time_remote > it.last_write_time_local) {
			//Remove and download file
			if(!std::remove((m_local_storage_path + it.filename).c_str())) {

				if(m_curl_wrapper.DownloadFile(it.filename)) {
					boost::filesystem::path file(m_local_storage_path + it.filename);
					UpdateFileInConfig(it.filename,
									   it.last_write_time_remote,
									   boost::filesystem::last_write_time(file));
				} else {
					std::cout << "Couldn't update file \"" << it.filename << "\"" << std::endl;
				}
			} else {
				std::cout << "Couldn't update file \"" << it.filename << "\"" << std::endl;
			}
		}
	}
}

void CloudStorage::uploadModifiedFiles(
		const Json::Value &storage_list,
		const std::vector<std::pair<std::string, std::time_t>> &local_storage_list) {
	LOG_D("Start uploadModifiedFiles()");
	auto list_of_identical_files = getListOfIdenticalFiles(storage_list, local_storage_list);

	for(const auto &it : list_of_identical_files) {
		if(it.last_write_time_remote < it.last_write_time_local) {
			if(m_curl_wrapper.UpdateFile(it.filename, it.last_write_time_local)) {
				UpdateFileInConfig(it.filename,
								   it.last_write_time_local,
								   it.last_write_time_local);
			} else {
				std::cout << "Couldn't upload modified file \"" << it.filename << "\"" << std::endl;
			}
		}
	}
}

std::vector<CloudStorage::IdenticalFile> CloudStorage::getListOfIdenticalFiles(
		const Json::Value &storage_list,
		const std::vector<std::pair<std::string, std::time_t>> &local_storage_list) {
	std::vector<IdenticalFile> list_of_identical_files;

	for(const auto &it_remote_storage : storage_list) {
		auto it_local_storage =
				std::find_if(local_storage_list.begin(),
							 local_storage_list.end(),
							 [=](const std::pair<std::string, std::time_t> &ifit) {
								 return ifit.first ==
										it_remote_storage["filename"].asString();
							 }
				);

		if(it_local_storage != local_storage_list.end()) {
			IdenticalFile file {};
			file.filename = it_local_storage->first;
			file.last_write_time_local = it_local_storage->second;
			file.last_write_time_remote =
					atoi(it_remote_storage["last_write_time"].asString().c_str());
			list_of_identical_files.push_back(file);
		}
	}

	return list_of_identical_files;
}

std::vector<std::pair<std::string, std::time_t>> CloudStorage::getListOfNewFiles(
		const Json::Value &storage_list,
		const std::vector<std::pair<std::string, std::time_t>> &local_storage_list) {
	std::vector<std::pair<std::string, std::time_t>> list_of_new_files;

	for(const auto &it_local_storage : local_storage_list) {
		auto it_remote_storage =
				std::find_if(storage_list.begin(),
							 storage_list.end(),
							 [=](const Json::Value &ifit) {
								 return ifit["filename"].asString() ==
										it_local_storage.first;
							 }
				);
		if(it_remote_storage == storage_list.end()) {
			auto pair = std::make_pair(it_local_storage.first, it_local_storage.second);
			list_of_new_files.push_back(pair);
		}
	}

	return list_of_new_files;
}

std::vector<std::pair<std::string, std::time_t>> CloudStorage::getListOfDeletedFiles(
		const Json::Value &storage_list,
		const std::vector<std::pair<std::string, std::time_t>> &local_storage_list) {
	std::vector<std::pair<std::string, std::time_t>> list_of_deleted_files;
	for(const auto &it_remote_storage : storage_list) {
		auto it_local_storage =
				std::find_if(local_storage_list.begin(),
							 local_storage_list.end(),
							 [=](const std::pair<std::string, std::time_t> &ifit) {
								 return ifit.first ==
										it_remote_storage["filename"].asString();
							 }
				);

		if(it_local_storage == local_storage_list.end()) {
			auto pair = std::make_pair(it_remote_storage["filename"].asString(),
									   atoi(it_remote_storage["last_write_time"].asString().c_str()));
			list_of_deleted_files.push_back(pair);
		}
	}

	return list_of_deleted_files;
}

void CloudStorage::downloadDeletedFiles(
		const Json::Value &storage_list,
		const std::vector<std::pair<std::string, std::time_t>> &local_storage_list) {
	LOG_D("Start downloadDeletedFiles()");
	auto deleted_files = getListOfDeletedFiles(storage_list, local_storage_list);
	for(const auto &it : deleted_files) {
		if(m_curl_wrapper.DownloadFile(it.first)) {
			boost::filesystem::path file(m_local_storage_path + it.first);
			AddFileToConfig(it.first,
							it.second,
							boost::filesystem::last_write_time(file));
		} else {
			std::cout << "Couldn't download file \"" << it.first << "\"" << std::endl;
		}
	}
}

void CloudStorage::uploadNewFiles(
		const Json::Value &storage_list,
		const std::vector<std::pair<std::string, std::time_t>> &local_storage_list) {
	LOG_D("Start uploadNewFiles()");
	auto new_files = getListOfNewFiles(storage_list, local_storage_list);
	for(const auto &it : new_files) {
		if(m_curl_wrapper.UploadFile(it.first, it.second)) {
			LOG_D(std::string("Success upload \"") + it.first + "\"");
			AddFileToConfig(it.first, it.second, it.second);
		} else {
			std::cout << "Couldn't upload file \"" << it.first << "\"" << std::endl;
		}
	}
}

bool CloudStorage::CheckExistsLocalStoragePath() {
	boost::filesystem::path local_storage(m_local_storage_path);
	if(!boost::filesystem::exists(local_storage) ||
	   !boost::filesystem::is_directory(local_storage)) {
		std::cout << "Couldn't find local storage directory. Path = " <<
				  m_local_storage_path;
		return false;
	}

	return true;
}

std::vector<std::pair<std::string, std::time_t>> CloudStorage::getLocalStorageList() {
	auto real_files = getRealLocalStorageList();
	std::vector<std::pair<std::string, std::time_t>> result_list;
	std::vector<std::string> files_to_delete;
	//Check deleted files and identical files
	for(auto &config_file : Config["files"]) {
		auto real_file = std::find_if(real_files.begin(),
									  real_files.end(),
									  [=](const std::pair<std::string, std::time_t> &if_file) {
										  return if_file.first == config_file["filename"].asString();
									  });
		if(real_file == real_files.end()) {
			//In case we are not deleting now, because then we will break Config["files"]
			files_to_delete.push_back(config_file["filename"].asString());
			//DeleteFileFromConfig(config_file["filename"].asString());
		} else if(config_file["real_last_write_time"] < real_file->second) {
			//file was changed
			//file will be update in config later
			result_list.emplace_back(real_file->first, real_file->second);
		} else {
			//file was not changed
			result_list.emplace_back(real_file->first, config_file["last_write_time"].asUInt64());
		}
	}

	for(const auto &file_to_delete : files_to_delete) {
		DeleteFileFromConfig(file_to_delete);
	}

	//Check new files
	for(auto &real_file : real_files) {
		auto config_file = std::find_if(Config["files"].begin(),
										Config["files"].end(),
										[=](const Json::Value &if_file) {
											return if_file["filename"].asString() == real_file.first;
										});
		if(config_file == Config["files"].end()) {
			//new file
			//file will be added later
			result_list.emplace_back(real_file.first, real_file.second);
		}
	}

	return result_list;
}

void CloudStorage::sendInfoAboutDeletedFiles(
		const Json::Value &storage_list,
		const std::vector<std::pair<std::string, std::time_t>> &local_storage_list) {
	LOG_D("Start sendInfoAboutDeletedFiles()");
	auto deleted_files = getListOfDeletedFiles(storage_list, local_storage_list);
	for(const auto &file : deleted_files) {
		if(m_curl_wrapper.DeleteFile(file.first)) {
			LOG_D("Success send info about deleted files");
		} else {
			LOG_D("Couldn't send info about deleted files");
		}
	}
}

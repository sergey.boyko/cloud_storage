//
// Created by bso on 18.02.18.
//

#ifndef CLOUD_STORAGE_CURLWRAPPER_H
#define CLOUD_STORAGE_CURLWRAPPER_H

#include <string>
#include <curl/curl.h>
#include <ctime>

#include "thirdparty/json.h"

class CurlWrapper {
public:
	CurlWrapper(const std::string &url, const std::string &local_storage_path);

	bool UploadFile(const std::string &file, std::time_t last_write_time);

	bool DownloadFile(const std::string &file);

	bool UpdateFile(const std::string &file, std::time_t last_write_time);

	bool GetRemoteStorageList(Json::Value &list);

	bool DeleteFile(const std::string &file);

private:
	static size_t readFile(void *ptr, size_t size, size_t nmemb, FILE *stream);

	static size_t readPage(char *ptr, size_t size, size_t nmemb, std::string *buffer);

	bool sendFile(const std::string &handler, const std::string &file, std::time_t last_write_time);

	/**
	* Rename source file to file with replaced '+' to ' '
	* @param filename
	* @return
	*/
	int renameFile(const std::string &filename);

	std::string replacedSymbols(const std::string &filename, char src_chr, char dst_chr);

	std::string m_json_buffer;

	std::string m_storage_url;
	std::string m_local_storage_path;
};

#endif //CLOUD_STORAGE_CURLWRAPPER_H
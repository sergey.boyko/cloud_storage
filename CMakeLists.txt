cmake_minimum_required(VERSION 3.5.1)
project(cloud_storage)

set(CMAKE_CXX_STANDARD 14)

find_package(Boost REQUIRED COMPONENTS system filesystem program_options)
include_directories(SYSTEM ${Boost_INCLUDE_DIR})

find_package(Threads)
include_directories(SYSTEM ${Threads_INCLUDE_DIR})

find_package(CURL REQUIRED)
include_directories(${CURL_INCLUDE_DIR})

add_executable(${PROJECT_NAME}
        main.cpp
        CurlWrapper.cpp
        CloudStorage.cpp
        Config.cpp
        thirdparty/jsoncpp.cpp
        ProgramLog.cpp)

target_link_libraries(${PROJECT_NAME} ${Boost_PROGRAM_OPTIONS_LIBRARY} ${Boost_SYSTEM_LIBRARY} ${Boost_FILESYSTEM_LIBRARY} ${CMAKE_THREAD_LIBS_INIT} ${CURL_LIBRARIES} ${OPENSSL_LIBRARIES})
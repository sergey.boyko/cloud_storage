//
// Created by bso on 21.02.18.
//

#pragma once

#ifndef CLOUD_STORAGE_PROGRAMLOG_H
#define CLOUD_STORAGE_PROGRAMLOG_H

#include <string>
#include <iostream>
#include <fstream>
#include <ctime>
#include <sstream>
#include <pwd.h>
#include <unistd.h>

void SetConsoleAllowed(int value);

int GetConsoleAllowed();

void SetLogPath(const std::string &path);

std::string GetLogPath();

std::string TimeNowAndFileSource(const std::string &file, int line);

std::ios_base::openmode GetWriteMode();

template <typename T>
void PrintLog(const std::string &file, int line, const T &message) {
	try {
		std::ofstream of;
		auto path = GetLogPath();
		if(!path.empty()) {
			of.open(path + "cloud_storage.log.debug.log", GetWriteMode());
		} else {
			struct passwd *pw = getpwuid(getuid());
			std::string home_dir(pw->pw_dir);
			of.open(home_dir + "/.cloud_storage.log.debug.log", GetWriteMode());
		}
		of << TimeNowAndFileSource(file, line) << message << std::endl;

		if(GetConsoleAllowed()) {
			std::cout << TimeNowAndFileSource(file, line) << message << std::endl;
		}
	}
	catch(const std::ios::failure &ec) {
	}
}

#define LOG_D(x) PrintLog(__FILE__,__LINE__,x)

#endif //CLOUD_STORAGE_PROGRAMLOG_H

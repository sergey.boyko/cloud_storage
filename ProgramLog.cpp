//
// Created by bso on 21.02.18.
//

#include "ProgramLog.h"

int IsConsoleAllowed = 0;
std::string LogPath;
int IsClearLog = 1;

void SetConsoleAllowed(int value) {
	IsConsoleAllowed = value;
}

int GetConsoleAllowed() {
	return IsConsoleAllowed;
}

void SetLogPath(const std::string &path) {
	if(*path.rbegin() != '/') {
		LogPath = path + "/";
	} else {
		LogPath = path;
	}
}

std::string GetLogPath() {
	return LogPath;
}

std::ios_base::openmode GetWriteMode() {
	if(IsClearLog) {
		IsClearLog = 0;
		return std::ios::out;
	}
	return std::ios::app;
}

std::string TimeNowAndFileSource(const std::string &file, int line) {
	std::stringstream time_stream;
	std::time_t curent_time = time(nullptr);
	struct tm tstruct {};
	tstruct = *localtime(&curent_time);

	time_stream << tstruct.tm_hour << ":" << tstruct.tm_min << ":" << tstruct.tm_sec;
	auto time_from_now = clock();
	std::string time = "[";

	time += time_stream.str() + ":" + std::to_string(time_from_now) + "]: " + file + ":" + std::to_string(line) + ": ";
	return time;
}
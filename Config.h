//
// Created by bso on 18.02.18.
//

#ifndef STUN_MONITOR_CONFIG_H
#define STUN_MONITOR_CONFIG_H

#include <ctime>
#include "thirdparty/json.h"

extern Json::Value Config;

int SetConfig();

bool ReadConfig();

bool AddFileToConfig(const std::string &filename,
					 std::time_t last_write_time,
					 std::time_t real_last_write_time);

bool DeleteFileFromConfig(const std::string &filename);

bool UpdateFileInConfig(const std::string &filename,
						std::time_t last_write_time,
						std::time_t real_last_write_time);

void PrintConfigToLog();

#endif //STUN_MONITOR_CONFIG_H

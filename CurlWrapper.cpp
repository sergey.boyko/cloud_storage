//
// Created by bso on 18.02.18.
//

#include <iostream>
#include <sys/stat.h>
#include <ctime>
#include "CurlWrapper.h"
#include "ProgramLog.h"

CurlWrapper::CurlWrapper(const std::string &url, const std::string &local_storage_path):
		m_storage_url(url),
		m_local_storage_path(local_storage_path) {
}

bool CurlWrapper::UploadFile(const std::string &file, std::time_t last_write_time) {
	return sendFile("upload.php", file, last_write_time);
}

bool CurlWrapper::DownloadFile(const std::string &file) {
	CURL *curl_handle = curl_easy_init();
	FILE *download_file = fopen((m_local_storage_path + file).c_str(), "wb");

	curl_easy_setopt(curl_handle, CURLOPT_URL, (m_storage_url +
												"files/" +
												replacedSymbols(file, ' ', '+')).c_str());
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, CurlWrapper::readFile);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, download_file);
	curl_easy_setopt (curl_handle, CURLOPT_VERBOSE, false);

	if(auto result = curl_easy_perform(curl_handle)) {
		LOG_D(std::string("CurlWrapper::DownloadFile() ") + curl_easy_strerror(result));
		return false;
	}

	if(renameFile(file)) {
		LOG_D("CurlWrapper::DownloadFile() Couldn't rename file \"" + file + "\"");
	}

	curl_easy_cleanup(curl_handle);
	return true;
}

bool CurlWrapper::UpdateFile(const std::string &file, std::time_t last_write_time) {
	return sendFile("update.php", file, last_write_time);
}

bool CurlWrapper::sendFile(const std::string &handler,
						   const std::string &file,
						   std::time_t last_write_time) {
	m_json_buffer.clear();
	CURL *curl_handle = curl_easy_init();
	FILE *upload_file = fopen((m_local_storage_path + file).c_str(), "rb");
	struct stat file_info {};

	if(!upload_file) {
		LOG_D(std::string("CurlWrapper::sendFile() Couldn't open file: ") +
			  m_local_storage_path +
			  file);
		return false;
	}

	if(fstat(fileno(upload_file), &file_info) != 0) {
		LOG_D(std::string("CurlWrapper::sendFile() Couldn't get file size: ") +
			  m_local_storage_path +
			  file);
		return false;
	}

	curl_easy_setopt(curl_handle,
					 CURLOPT_URL,
					 (m_storage_url +
					  handler +
					  "?filename=" +
					  replacedSymbols(file, ' ', '+') +
					  "&last_write_time=" +
					  std::to_string(last_write_time)).c_str());
	curl_easy_setopt(curl_handle,
					 CURLOPT_INFILE,
					 upload_file);
	curl_easy_setopt(curl_handle, CURLOPT_INFILESIZE, (curl_off_t)file_info.st_size);
	curl_easy_setopt(curl_handle, CURLOPT_PUT, true);
	curl_easy_setopt(curl_handle, CURLOPT_UPLOAD, true);
	curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, true);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, CurlWrapper::readPage);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, &m_json_buffer);
	curl_easy_setopt (curl_handle, CURLOPT_VERBOSE, false);

	if(auto result = curl_easy_perform(curl_handle)) {
		LOG_D(std::string("CurlWrapper::sendFile() ") + curl_easy_strerror(result));
		return false;
	}

	curl_easy_cleanup(curl_handle);
	if(m_json_buffer != "success") {
		LOG_D("CurlWrapper::sendFile() Received: ");
		LOG_D(m_json_buffer);
		return false;
	}
	return true;
}

bool CurlWrapper::GetRemoteStorageList(Json::Value &list) {
	m_json_buffer.clear();
	CURL *curl_handle = curl_easy_init();
	curl_easy_setopt(curl_handle, CURLOPT_URL, (m_storage_url + "getlist.php").c_str());
	//curl_easy_setopt(curl_handle, CURLOPT_HEADER, true);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, CurlWrapper::readPage);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, &m_json_buffer);
	curl_easy_setopt (curl_handle, CURLOPT_VERBOSE, false);
	auto result = curl_easy_perform(curl_handle);

	if(result != CURLE_OK) {
		LOG_D(std::string("CurlWrapper::GetRemoteStorageList() ") +
			  curl_easy_strerror(result));
		return false;
	}

	if(m_json_buffer == "fail") {
		LOG_D(std::string("CurlWrapper::GetRemoteStorageList() Received: ") + m_json_buffer);
		return false;
	}

	std::istringstream is(m_json_buffer);

	try {
		is >> list;
		Json::StyledWriter writer;
		LOG_D("CurlWrapper::GetRemoteStorageList() Json: " + writer.write(list));
	}
	catch(const Json::RuntimeError &er) {
		LOG_D("CurlWrapper::GetRemoteStorageList() Invalid download json");
		LOG_D(std::string("Json: ") + m_json_buffer);
		return false;
	}
	catch(const std::exception &ex) {
		LOG_D("CurlWrapper::GetRemoteStorageList() Invalid download json");
		LOG_D(std::string("Json: ") + m_json_buffer);
		return false;
	}
	return true;
}

size_t CurlWrapper::readFile(void *ptr, size_t size, size_t nmemb, FILE *stream) {
	size_t written = fwrite(ptr, size, nmemb, stream);
	return written;
}

size_t CurlWrapper::readPage(char *ptr, size_t size, size_t nmemb, std::string *buffer) {
	auto res = size * nmemb;
	buffer->append(ptr, res);
	return res;
}

bool CurlWrapper::DeleteFile(const std::string &file) {
	m_json_buffer.clear();
	CURL *curl_handle = curl_easy_init();
	curl_easy_setopt(curl_handle,
					 CURLOPT_URL,
					 (m_storage_url +
					  "delete.php?filename=" +
					  replacedSymbols(file, ' ', '+')).c_str());
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, CurlWrapper::readPage);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, &m_json_buffer);
	curl_easy_setopt (curl_handle, CURLOPT_VERBOSE, false);
	auto result = curl_easy_perform(curl_handle);

	if(result != CURLE_OK) {
		LOG_D(std::string("CurlWrapper::DeleteFileFromConfig() ") +
			  curl_easy_strerror(result));
		return false;
	}

	if(m_json_buffer != "success") {
		LOG_D(std::string("CurlWrapper::DeleteFileFromConfig() Received: ") + m_json_buffer);
		return false;
	}

	return true;
}

int CurlWrapper::renameFile(const std::string &filename) {
	//Rename download file
	return std::rename((m_local_storage_path + filename).c_str(),
					   (m_local_storage_path + replacedSymbols(filename,
															   '+', ' ')).c_str());
}

std::string CurlWrapper::replacedSymbols(const std::string &filename,
										 char src_chr,
										 char dst_chr) {
	//real_filename is file, where '+' replaced to ' '
	std::string real_filename(filename);
	for(auto &simb : real_filename) {
		if(simb == src_chr) {
			simb = dst_chr;
		}
	}

	return real_filename;
}

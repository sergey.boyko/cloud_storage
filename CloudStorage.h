//
// Created by bso on 18.02.18.
//

#ifndef CLOUD_STORAGE_CLOUDSTORAGE_H
#define CLOUD_STORAGE_CLOUDSTORAGE_H

#include <string>
#include "CurlWrapper.h"

class CloudStorage {
public:
	CloudStorage(const std::string &url, const std::string &local_storage_path);

	int ProcessPull();

	int ProcessPush();

	bool CheckExistsLocalStoragePath();

private:
	struct IdenticalFile {
		std::string filename;
		std::time_t last_write_time_local;
		std::time_t last_write_time_remote;
	};

	void updateModifiedFiles(
			const Json::Value &storage_list,
			const std::vector<std::pair<std::string, std::time_t>> &local_storage_list);

	void uploadModifiedFiles(
			const Json::Value &storage_list,
			const std::vector<std::pair<std::string, std::time_t>> &local_storage_list);

	void downloadDeletedFiles(
			const Json::Value &storage_list,
			const std::vector<std::pair<std::string, std::time_t>> &local_storage_list);

	void uploadNewFiles(
			const Json::Value &storage_list,
			const std::vector<std::pair<std::string, std::time_t>> &local_storage_list);

	void sendInfoAboutDeletedFiles(const Json::Value &storage_list,
								   const std::vector<std::pair<std::string, std::time_t>> &local_storage_list);

	std::vector<std::pair<std::string, std::time_t>> getRealLocalStorageList();

	/**
	* Get list of new files, which are not present on the server
	* @param storage_list
	* @return
	*/
	std::vector<std::pair<std::string, std::time_t>> getListOfNewFiles(
			const Json::Value &storage_list,
			const std::vector<std::pair<std::string, std::time_t>> &local_storage_list);

	/**
	* Get list of deleted files, which the client does not have
	* @param storage_list
	* @return
	*/
	std::vector<std::pair<std::string, std::time_t>> getListOfDeletedFiles(
			const Json::Value &storage_list,
			const std::vector<std::pair<std::string, std::time_t>> &local_storage_list);

	/**
	* Get list of identical client and server files
	* @param storage_list
	* @return
	*/
	std::vector<CloudStorage::IdenticalFile> getListOfIdenticalFiles(
			const Json::Value &storage_list,
			const std::vector<std::pair<std::string, std::time_t>> &local_storage_list);

	std::vector<std::pair<std::string, std::time_t>> getLocalStorageList();

	CurlWrapper m_curl_wrapper;
	std::string m_storage_url;

	std::string m_local_storage_path;
};

#endif //CLOUD_STORAGE_CLOUDSTORAGE_H
//
// Created by bso on 18.02.18.
//

#include <boost/program_options.hpp>
#include <iostream>
#include <fstream>

#include "CloudStorage.h"
#include "Config.h"
#include "ProgramLog.h"

namespace po = boost::program_options;

int main(int argc, char *argv[]) {
	std::string action;
	SetConsoleAllowed(1);
	SetLogPath("/home/bso/tmp/");
	LOG_D("Start application");

	po::options_description general_options("General options");
	general_options.add_options()
			("help,h", "Show help")
			("action,a",
			 po::value<std::string>(&action)->required(),
			 R"(Enter action: "pull" to pull from storage; "push" to push differences to storage; "tune" to tune application)");

	po::variables_map vm;

	try {
		po::store(po::parse_command_line(argc, argv, general_options), vm);

		if(vm.count("help")) {
			std::cout << general_options << std::endl;
			return 0;
		}

		po::notify(vm);

		if(action != "tune" && action != "pull" && action != "push") {
			std::cout << "Please, enter action right";
			std::cout << general_options << std::endl;
			return 0;
		}

		if(action == "tune") {
			return SetConfig();
		}

		ReadConfig();

		CloudStorage storage(Config["storage_url"].asString(),
							 Config["local_storage_path"].asString());
		if(!storage.CheckExistsLocalStoragePath()) {
			return 1;
		}

		if(action == "push") {
			return storage.ProcessPush();
		}

		if(action == "pull") {
			return storage.ProcessPull();
		}
	}
	catch(const boost::program_options::error &ec) {
		std::cerr << std::endl << __PRETTY_FUNCTION__ << ":" << __FILE__ << ":" << __LINE__
				  << " Could't parse command line: " << ec.what() << std::endl;
		return 1;
	}
	catch(const std::runtime_error &err) {
		std::cerr << std::endl << __PRETTY_FUNCTION__ << ":"
				  << __FILE__ << ":" << __LINE__ << ": " << err.what()
				  << std::endl;
		return 1;
	}
	catch(const std::exception &ex) {
		std::cerr << std::endl << __PRETTY_FUNCTION__ << ":" << __FILE__ << ":"
				  << __LINE__ << ": " << ex.what() << std::endl;
		return 1;
	}
	return 0;
}
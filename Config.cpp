//
// Created by bso on 18.02.18.
//

#include <string>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <pwd.h>
#include <algorithm>

#include "Config.h"
#include "ProgramLog.h"

Json::Value Config;

std::string getConfigPath() {
	struct passwd *pw = getpwuid(getuid());
	std::string home_dir(pw->pw_dir);
	return home_dir + "/.storage.json";
}

bool updateConfig() {
	try {
		std::ofstream os(getConfigPath());
		if(!os) {
			return false;
		}

		os << Config;
		os.close();
		return true;
	} catch(const std::ofstream::failure &err) {
		return false;
	}
}

void PrintConfigToLog() {
	Json::StyledWriter writer;
	LOG_D(writer.write(Config));
}

int SetConfig() {
	std::string url, local_storage_path;
	std::cout << "Enter storage url" << std::endl;
	std::getline(std::cin, url);

	std::cout << "Enter local storage path" << std::endl;
	std::getline(std::cin, local_storage_path);

	try {
		struct passwd *pw = getpwuid(getuid());
		std::string home_dir(pw->pw_dir);
		std::ofstream os(home_dir + "/.storage.json");
		if(!os) {
			std::cerr << "Couldn't create config" << std::endl;
			return 1;
		}
		Json::Value root;

		if(*url.rbegin() != '/') {
			url.push_back('/');
		}

		if(*local_storage_path.rbegin() != '/') {
			local_storage_path.push_back('/');
		}

		root["storage_url"] = url;
		root["local_storage_path"] = local_storage_path;

		root["files"] = Json::Value(Json::arrayValue);

		os << root;
		os.close();
		return 0;
	} catch(const std::ifstream::failure &e) {
		std::cerr << "Couldn't create config: " << e.what();
		return 1;
	}
}

bool ReadConfig() {
	Json::Reader reader;
	std::ifstream in;
	try {
		in.open(getConfigPath());
	} catch(const std::ifstream::failure &e) {
		std::cerr << "Couldn't open config";
		return false;
	}

	if(!reader.parse(in, Config)) {
		std::cerr << "Couldn't parse config";
		return false;
	}

	in.close();
	return true;
}

bool AddFileToConfig(const std::string &filename,
					 std::time_t last_write_time,
					 std::time_t real_last_write_time) {
	auto &file = Config["files"].append(Json::Value(Json::objectValue));
	file["filename"] = filename;
	file["last_write_time"] = last_write_time;
	file["real_last_write_time"] = real_last_write_time;

	if(!updateConfig()) {
		LOG_D("Couldn't add file to config");
		return false;
	}
	LOG_D("Success add file to config");
	//PrintConfigToLog();
	return true;
}

bool DeleteFileFromConfig(const std::string &filename) {
	auto k = 0;
	for(auto &it : Config["files"]) {
		if(it["filename"] == filename) {
			break;
		}
		++k;
	}
	if(k == Config["files"].size()) {
		LOG_D("Couldn't delete file from config");
		return false;
	}

	Json::Value removable_file;
	Config["files"].removeIndex(static_cast<Json::ArrayIndex>(k), &removable_file);

	if(!updateConfig()) {
		LOG_D("Couldn't delete file from config");
		return false;
	}
	LOG_D("Success delete file from config:");
	return true;
}

bool UpdateFileInConfig(const std::string &filename,
						std::time_t last_write_time,
						std::time_t real_last_write_time) {
	auto it = std::find_if(Config["files"].begin(),
						   Config["files"].end(), [=](const Json::Value &file_it) {
				return file_it["filename"] == filename;
			});
	if(it == Config["files"].end()) {
		LOG_D("Couldn't delete file from config");
		return false;
	}

	auto &file = *it;
	file["last_write_time"] = last_write_time;
	file["real_last_write_time"] = real_last_write_time;

	if(!updateConfig()) {
		LOG_D("Couldn't update file in config");
	}
	LOG_D("Success update file in config:");
	return true;
}